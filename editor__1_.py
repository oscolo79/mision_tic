from tkinter import *

root = Tk()
root.title(" Mi editor de texto ")

menubar = Menu(root)
root.config(menu = menubar)

filemenu = Menu(menubar, tearoff=0)
filemenu.add_command(label="Nuevo")
filemenu.add_command(label="Abrir")
filemenu.add_command(label="Guardar")
filemenu.add_command(label="Cerrar")
filemenu.add_separator()
filemenu.add_command(label="Salir", command=root.quit)
menubar.add_cascade(menu = filemenu, label = "Archivo")

texto = Text(root)
texto.pack(fill = "both", expand=1)
texto.config(bd=0, padx=6, pady=4, font=("Consolas", 12))

mensaje = StringVar()
mensaje.set("Bienvenido a mi editor de texto")
monitor = Label(root, textvar = mensaje, justify="left")
monitor.pack(side="left")

root.mainloop()

